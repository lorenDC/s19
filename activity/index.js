
// S19 - Javascript - Selection Control Structures

/*ACTIVITY*/


/*

	1. Declare 3 global variables without initialization called username,password and role.
	2. Create a login function which is able to prompt the user to provide their username, password and role.
		a.) use prompt() and update the username,password and role global variables with the prompt() returned values.
		b.) add an if statement to check if the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
			** if it is, show an alert to inform the user that their input should not be empty.
		c.) Add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
			** if the user's role is admin, show an alert with the following message:
						"Welcome back to the class portal, admin!"
			** if the user's role is teacher, show an alert with the following message:
						"Thank you for logging in, teacher!"
			** if the user's role is a student, show an alert with the following message:
 						"Welcome to the class portal, student!"
			** if the user's role does not fall under any of the cases, as a default, show a message:
 						"Role out of range."
*/


	// Code here:
	let user;
	let pass;
	let role;

	function loginUser(){

		user = prompt("Enter your username: ");
		pass = prompt("Enter your password: ");
		role = prompt("Enter your role: ");

		if (!user || !pass || !role){
			alert("Invalid Credentials");
		} else {
			switch(role){
				case "Admin":
				alert("Welcome back to the class portal, admin!");
				break;

				case "Teacher":
				alert("Thank you for logging in, teacher!");
				break;

				case "Student":
				alert("Welcome to the class portal, student!");
				break;

				default:
				alert("Role out of range.");
				break;
			}
		}
};

loginUser();
		

/*
	3. Create a function which is able to receive 4 numbers as arguments, calculate its average and log a message for  the user about their letter equivalent in the console.
		a.) add parameters appropriate to describe the arguments.
		b.)create a new function scoped variable called average.
		c.) calculate the average of the 4 number inputs and store it in the variable average.
		d.)research the use of Math.round() and round off the value of the average variable.
			**update the average variable with the use of Math.round()
			**console.log() the average variable to check if it is rounding off first.

	4. add an if statement to check if the value of avg is less than or equal to 74.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is F"

	5. add an else if statement to check if the value of avg is greater than or equal to 75 and if average is less than or equal to 79.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is D"

	6. add an else if statement to check if the value of avg is greater than or equal to 80 and if average is less than or equal to 84.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is C"

	7. add an else if statement to check if the value of avg is greater than or equal to 85 and if average is less than or equal to 89.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is B"

	8. add an else if statement to check if the value of avg is greater than or equal to 90 and if average is less than or equal to 95.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is A"

	9. add an else if statement to check if the value of average is greater than 96.
		a.) if it is, show the following message in a console.log():
			"Hello, student, your average is <show average>. The letter equivalent is A+"

*/


	// Code here:

	function checkAverage(num1, num2, num3, num4){
		let average = (num1+num2+num3+num4)/4
		return(Math.round(average));
	};

		let param1 = parseFloat(prompt("Enter number:"));
		console.log(param1);
		let param2 = parseFloat(prompt("Enter number:"));	
		console.log(param2);
		let param3 = parseFloat(prompt("Enter number:"));
		console.log(param3);
		let param4 = parseFloat(prompt("Enter number:"));
		console.log(param4);

		let getAverage = checkAverage(param1, param2, param3, param4)
		console.log("The average is:");
		console.log(getAverage);

	if(getAverage <= 74){
        console.log("Hello, student, your average is " +  getAverage + ". The letter equivalent is F");
    }else if(getAverage >= 75 && getAverage <= 79){
        console.log("Hello, student, your average is " +  getAverage + ". The letter equivalent is D");

    }else if(getAverage >= 80 && getAverage <= 84){
        console.log("Hello, student, your average is " +  getAverage + ". The letter equivalent is C");

    }else if(getAverage >= 85 && getAverage <= 89){
        console.log("Hello, student, your average is " +  getAverage + ". The letter equivalent is B");

    }else if(getAverage >= 90 && getAverage <= 95){
        console.log("Hello, student, your average is " +  getAverage + ". The letter equivalent is A");

    }else {
        console.log("Hello, student, your average is " +  getAverage + ". The letter equivalent is A+");
    }
